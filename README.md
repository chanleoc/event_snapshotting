# README #
This repository is a collection of functions which allows users to capture and snapshot changes or events within a dataset.

### Component Setup ###

- Initial Setup
    - Since Snapshot table does not exist, a table mapping for it will be unnecessary. Component will output a Snapshot Table with the provided data table to the default bucket(in.c-snapshots.). Although table mapping is not required for snapshot table, it is mandatory to enter the data table mapping as the component needs to recognize which data to snapshot.
    - Initial Snapshot table will be outputed with the "snapshot\_table\_name" provided in the configuration. Therefore, "snapshot\_table\_name" can never be null.

- Snapshotting Setup
    - With the Snapshot table in the input mapping, component will automatically fetch, update and output into the same snapshot table destination. 
    - If user wants to add more observes, user can always add the additional observe in the configuration when they want to.

### Configurations ###

1. Data Table Name
    - The input table which will be used to compare with the snapshot table for any updates or changes.
2. Snapshot Table Name
    - The input/output table which will capture updates or changes from comparison with data_table.
    - If Snapshot Table Name is not found in input mapping, program will automatically transform and output the data\_table with the requested parameters; However, a Snapshot\_Table\_Name will still have to be specified in the configuration.
    - If Snapshot\_Table\_Name is specified in input mapping, program will automatically output the results back into the same destination of Snapshot\_Table\_Name.
3. Include Date Only
    - Description of when the snapshot was taken
    - "YES" = output column will only display dates
    *NOTE: If the same row is being updated within the same date, the component will update the values only  instead of inserting a new row.*
    - "NO" = output column will contain the time of the event
    - Default: "YES"
4. Incrumental Update
    - Component behaviour on "active" rows in snapshotting file which are missing from the data input file
    - "YES": "active" rows in snapshotting file which are not found in data input files will stay active (actual = 1)
    - "NO": "active" rows in snapshotting file which are not found in data input files will be inactive (actual = 0)
    - Default: "NO"
5. Primary Keys
    - Columns which can be uniquely identified
    - If more than one primary keys are specified, program will combine all of the columns as one and output the new primary key under column ("Primary_Key")
    - Input column names has to be seperated with common delimiter
6. Observes   
    - Columns which the program will monitor for changes or updates
    - If no observes column is specified, component will treat all columns other than primary keys as observes variables
    - Input column names has to be seperated with common delimiter


### RAW JSON Configuration ###

**NEW**
```
{
    "data_table":"DATA_TABLE.csv",
    "snapshot_table":"SNAPSHOT_TABLE.csv",
    "date_only":"YES",
    "incrumental_update":"NO",
    "primarykey":"ID, Location",
    "observe":"Quantity, Manufacture, Price"
}
```
**DEPRECATED**
```
{
    "data_table":"DATA_TABLE.csv",
    "snapshot_table":"SNAPSHOT_TABLE.csv",
    "primarykey":[
        "ID"
    ],
    "observe":[
        "Quantity",
        "Manufacture",
        "Price"
    ],
    "date_only":"YES"
}
```

### Contact Info ###

Leo Chan  
Vancouver, Canada (PST time)  
Email: leo@keboola.com  
Private: cleojanten@hotmail.com  
