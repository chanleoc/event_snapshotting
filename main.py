import logging_gelf.handlers
import logging_gelf.formatters
import datetime
import dateparser
import pandas as pd
from keboola import docker
import json
import csv
import logging
import os
import sys
"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"

"""
Python 3 environment 
Event SnapShotting
"""

# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

# Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

logger = logging.getLogger()
logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
    host=os.getenv('KBC_LOGGER_ADDR'),
    port=int(os.getenv('KBC_LOGGER_PORT'))
)
logging_gelf_handler.setFormatter(
    logging_gelf.formatters.GELFFormatter(null_character=True))
logger.addHandler(logging_gelf_handler)

# removes the initial stdout logging
logger.removeHandler(logger.handlers[0])

logging.info("Docker Version: 0.2.4")


# destination to fetch and output files
DEFAULT_FILE_INPUT = "/data/in/tables/"
DEFAULT_FILE_DESTINATION = "/data/out/tables/"

# Access the supplied rules
cfg = docker.Config('/data/')
params = cfg.get_parameters()
data_table = cfg.get_parameters()["data_table"]
snapshot_table = cfg.get_parameters()["snapshot_table"]
primarykey = cfg.get_parameters()["primarykey"]
observe = cfg.get_parameters()["observe"]
only_date = cfg.get_parameters()["date_only"]
incrumental_update = cfg.get_parameters()["incrumental_update"]

# setting up date parameters
current_date = dateparser.parse("now")
future_date = datetime.datetime(9999, 12, 31, 00, 00, 00)
if only_date == "YES":
    date_only = True
    current_date_string = current_date.strftime("%Y-%m-%d")
    future_date_string = future_date.strftime("%Y-%m-%d")
else:
    date_only = False
    current_date_string = current_date.strftime("%Y-%m-%d %H:%M:%S")
    future_date_string = future_date.strftime("%Y-%m-%d %H:%M:%S")

# setting up incrumental parameters
if incrumental_update == "YES":
    incrumental = True
else:
    incrumental = False

# Get proper list of tables
cfg = docker.Config('/data/')
in_tables = cfg.get_input_tables()
out_tables = cfg.get_expected_output_tables()
logging.info("IN tables mapped: "+str(in_tables))
logging.info("OUT tables mapped: "+str(out_tables))

# verify if the required files are received
number_of_files = len(in_tables)
if not(number_of_files == 1 or number_of_files == 2):
    # verify the number of files
    logging.error("Please insert the required files for SnapShotting.")
    logging.error("Exit.")
    sys.exit(0)

# verify if the input file name exists in the input mapping
for i in in_tables:
    if data_table in i["full_path"]:
        pass
    elif snapshot_table in i["full_path"]:
        pass
    else:
        logging.error("Please confirm input file names.")
        logging.error("Exit.")
        sys.exit(0)


def get_tables(in_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    # input file
    table = in_tables[0]
    in_name = table["full_path"]
    in_destination = table["destination"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))
    if number_of_files == 1:
        return in_name
    elif number_of_files == 2:
        table = in_tables[1]
        in_name_1 = table["full_path"]
        in_destination = table["destination"]
        logging.info("Data table: " + str(in_name))
        logging.info("Input table source: " + str(in_destination))
        return in_name, in_name_1


def get_output_tables(out_tables):
    """
    Evaluate output table names.
    Only taking the first one into consideration!
    """

    # input file
    table = out_tables[0]
    in_name = table["full_path"]
    in_destination = table["source"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))

    return in_name


def dates_request(start_date, end_date):
    """
    return a list of dates within the given parameters
    """

    dates = []
    try:
        start_date_form = dateparser.parse(start_date)
        end_date_form = dateparser.parse(end_date)
        day_diff = (end_date_form-start_date_form).days
        if day_diff < 0:
            logging.error("ERROR: start_date cannot exceed end_date.")
            logging.error("Exit.")
            sys.exit(0)
        temp_date = start_date_form
        day_n = 0
        if day_diff == 0:
            # normal with leading zeros
            dates.append(temp_date.strftime("%Y-%m-%d"))
            # no leading zeros
            # dates.append(temp_date.strftime("%Y/%-m/%-d"))
        while day_n < day_diff:
            # normal with leading zeros
            dates.append(temp_date.strftime("%Y-%m-%d"))
            # no leading zeros
            # dates.append(temp_date.strftime("%Y/%-m/%-d"))
            temp_date += datetime.timedelta(days=1)
            day_n += 1
            if day_n == day_diff:
                # normal with leading zeros
                dates.append(temp_date.strftime("%Y-%m-%d"))
                # no leading zeros
                # dates.append(temp_date.strftime("%Y/%-m/%-d"))
    except TypeError:
        logging.error("ERROR: Please enter valid date parameters")
        logging.error("Exit.")
        sys.exit(1)

    return dates


def transform(df, observes):
    """
    Transformation: unpivoting the table 
    """

    try:
        observes.remove('Primary_Key')
    except Exception:
        pass
    data = pd.melt(df, id_vars=['Primary_Key'],
                   value_vars=observes, var_name="column")
    # adding start_date, end_date and actual column

    data['start_date'] = current_date_string
    data['end_date'] = future_date_string
    data['actual'] = 1

    return data


def snapshot_1(new_df, output_df, observes):
    """
    Snapshotting
    compare with the existing list and update the columna and rows accordinly
    Options:    updating every rows which has actual value as 1.
                 if same column value is found, it will ignore the process
                 if no new inputs are found and actual value is 1, actual value will become 0
    new_df = new input values
    output_df = the existing snapshotting file
    """

    temp_output = output_df  # used to make changes to the current snapshotting file
    # used to store new input files if no match found
    temp_input = pd.DataFrame(columns=list(temp_output))

    actual_df = output_df[(output_df["actual"] == "1")]

    for i in observes:
        # Searching values based on their column names first

        logging.info("Searching column: {0}".format(i))
        select_1 = actual_df[actual_df["column"] == i]
        input_df = new_df[new_df["column"] == i]
        input_df_index = input_df.index

        itr = 0
        while itr < len(input_df):
            # iternating very row of the new input file with the qualified column names
            current_row = input_df.iloc[itr]

            j = 0
            df = select_1[select_1["Primary_Key"]
                          == current_row["Primary_Key"]]

            if len(df) > 1:
                # if there are multiple rows detected in the search, terminating the program.
                logging.error(
                    "There is something wrong with the existing snapshotting file.")
                logging.error(
                    "Multiple rows have the same primary keys and actual value.")
                logging.error("Primary Key: {0}".format(
                    current_row["Primary_Key"]))
                logging.error("Please verify snapshotting input file.")
                logging.error("Exit.")
                sys.exit(0)

            if len(df) == 0:
                # when there are no matches, append the row into the temp_output
                temp_input = temp_input.append(current_row)
                logging.info(
                    "{0} - {1}: New row ".format(current_row["Primary_Key"], i))

            if len(df) == 1:
                # if there is a match
                # ignore if the same value exist in the list
                # append if value doesnt match and replace the actual to 0
                # incrumental_update = True: if values with actual 1 does not appear in input data, no change to actual
                # incrumental_update = False: if vlaues with actual 1 does not appear in input data, change actual to 0

                index = df.index[0]

                value = temp_output['value'][index]
                new_value = current_row['value']

                if str(value) != str(new_value):
                    # if value does not exist
                    logging.info(
                        "{0} - {1}: Appending changes ".format(current_row["Primary_Key"], i))

                    insert_bool = True
                    if date_only:
                        if temp_output['start_date'][index] == current_date_string:
                            # if changes happen within the same day, only values will be updated
                            # No new rows will be inserted
                            #temp_output['end_date'][index] = current_date_string
                            temp_output['value'][index] = new_value
                            insert_bool = False
                        else:
                            temp_output['end_date'][index] = (
                                current_date - datetime.timedelta(days=1)).strftime("%Y-%m-%d")

                    else:
                        temp_output['end_date'][index] = (
                            current_date - datetime.timedelta(seconds=1)).strftime("%Y-%m-%d %H:%M:%S")

                    if insert_bool:
                        temp_output['actual'][index] = 0
                        temp_input = temp_input.append(current_row)

                actual_df = actual_df.drop(index)
            itr += 1

    output_df = temp_output
    if not incrumental:
        # changing actual (1->0) if active rows from snapshot files are not found from the input files
        output_df = actual_change(actual_df.index, output_df)
    output_df = output_df.append(temp_input)

    return output_df


def actual_change(index_list, df):
    """
    Changing all the actual values from 1 to 0 and date to current for the provided df with the indexes provided
    """

    for i in index_list:
        df['actual'][i] = 0

        if date_only:
            if df['start_date'][i] == current_date_string:
                df['end_date'][i] = current_date_string
            else:
                df['end_date'][i] = (
                    current_date - datetime.timedelta(days=1)).strftime("%Y-%m-%d")
        else:
            df['end_date'][i] = (
                current_date - datetime.timedelta(seconds=1)).strftime("%Y-%m-%d %H:%M:%S")

    return df


def load_json(file_in):
    """
    Loading the input files as json (**Locally**)
    """

    with open(file_in, 'r') as json_data:
        data = json.load(json_data)
    return data


def produce_manifest(file_name, destination, header, manifest_pk):
    """
    Dummy function to produce manifest file.
    """

    file = DEFAULT_FILE_DESTINATION + file_name + ".manifest"

    manifest_template = {  # "source": "myfile.csv",
        "destination": "in.c-mybucket.table",
        "incremental": True,
        "primary_key": [],
        # "columns": [""],
        "delimiter": ","
        # "enclosure": ""
    }

    manifest = manifest_template
    #manifest["columns"] = header
    manifest["destination"] = destination
    manifest["primary_key"] = manifest_pk
    logging.info("Output ({0}) Manifest: {1}".format(file, manifest))

    try:
        with open(file, 'w') as file_out:
            json.dump(manifest, file_out)
            logging.info("Output manifest file produced.")
    except Exception as e:
        logging.error("Could not produce output file manifest.")
        logging.error(e)

    return


def read_manifest(file_name):
    """
    Dummy function to read manifest file.
    Purpose: Extracting the bucket destination and column headers
    """

    file = DEFAULT_FILE_INPUT + file_name + ".manifest"

    manifest_data = load_json(file)
    header = manifest_data["columns"]
    destination = manifest_data["id"]
    manifest_pk = manifest_data["primary_key"]

    return destination, header, manifest_pk


def manifest_handler():
    """
    Handles all the manifest processes
    """

    if number_of_files == 2:
        destination, header, manifest_pk = read_manifest(snapshot_table)
    elif number_of_files == 1:
        table_name = snapshot_table.split(".csv")[0]
        destination = "in.c-snapshots." + table_name
        header = ["Primary_Key", "column", "value",
                  "start_date", "end_date", "actual"]
        manifest_pk = ["Primary_Key", "column", "start_date"]

    produce_manifest(snapshot_table, destination, header, manifest_pk)

    return


def string_delimiter(string, delimiter):
    """
    Delimit a string into a list using the provided delimiter
    """

    string_list = string.split(delimiter)
    output_list = []
    for i in string_list:
        output_list.append(i.lstrip())

    return output_list


def main():
    """
    Main Execution Script
    """

    # import input table
    input_table = DEFAULT_FILE_INPUT+data_table
    if number_of_files == 2:
        file_to_compare = DEFAULT_FILE_INPUT+snapshot_table
        output = pd.read_csv(file_to_compare, dtype=str)
    output_table = DEFAULT_FILE_DESTINATION+snapshot_table
    data = pd.read_csv(input_table, dtype=str)

    # Delimiting the Primary Key string and the Observe string
    if primarykey:
        pk = string_delimiter(primarykey, ",")
    else:
        pk = []

    if observe:
        obs = string_delimiter(observe, ",")
    else:
        obs = []

    # Adding Primary Key column
    # if not specify, program will exit with an error message
    first = 0
    if len(pk) == 0:
        logging.error("Please specify your primary keys.")
        logging.error("Exit.")
        sys.exit(0)
    else:
        logging.info("Primary Keys: {0}".format(primarykey))
        for i in pk:
            if first == 0:
                data['Primary_Key'] = data[i]
                first += 1
            else:
                data['Primary_Key'] = data['Primary_Key']+"-"+data[i]

    # Determine the list of observe variables
    # if not specify, it will consider any columns which are not primary key to be the observe variables
    value_vars = []
    if len(obs) != 0:
        value_vars = obs
    else:
        value_vars = list(data)
        for i in pk:
            value_vars.remove(i)
    logging.info("Observe Variables: {0}".format(value_vars))

    # Transformation
    # Unpivoting the table
    logging.info("Transforming new file...")
    data_1 = transform(data, value_vars)

    # Snapshotting
    if number_of_files == 2:
        logging.info("Snapshotting changes...")
        output = snapshot_1(data_1, output, value_vars)

    # File output
    logging.info("Outputing file...")
    if number_of_files == 1:
        data_1.to_csv(output_table, index=False)
    else:
        output.to_csv(output_table, index=False)

    return


if __name__ == "__main__":

    manifest_handler()
    main()

    logging.info("Done.")
